public class Application {
    public static void main(String[] args) {

        DataObject data = new DataObject();
        data.setName("Name");
        data.setSurname("Surname");

        System.out.println(data.getName());
        System.out.println(data.getSurname());
    }
}

// All Lombok features can be found here: https://projectlombok.org/features/all
