import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataObject {
    private String name;
    private String surname;
}
